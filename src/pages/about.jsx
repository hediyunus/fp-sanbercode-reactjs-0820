
import React from "react"
import '../public/css//style.css';
import { Link } from "react-router-dom";

const About = () => {
    return (
        <section>
            <div style={{ padding: "10px", border: "1px solid #ccc "}}>
                <h1>Data Peserta Sanbercode Bootcamp Reactjs</h1>
                <ol>
                    <li><strong style={{ width: "100px"}}>Nama:</strong> Hedi Yunus</li>
                    <li><strong style={{ width: "100px"}}>Email:</strong> hediyunus26@gmail.com</li>
                    <li><strong style={{ width: "100px"}}>Sistem Operasi yang digunakan:</strong> Windows 10</li>
                    <li><strong style={{ width: "100px"}}>Akun Gitlab:</strong> @hediyunus</li>
                    <li><strong style={{ width: "100px"}}>Akun Telegram:</strong> @hediyunus</li>
                </ol>
            </div>
            <br />
            <br />
            <Link to="/">Kembali ke Home</Link>
            </section>
    );
};

export default About;
